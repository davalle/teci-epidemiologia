import matplotlib
import matplotlib.pyplot as plt

#tiempo
t0 = 0
tmax = 20
step = 1

#variables de estado
n = 763
s = 762
i = 1
r = 0

#datos
beta = 0.00218
gamma = 0.4404

#historial de estados
ss = []
ii = []
rr = []

ss.append(s) 
ii.append(i) 
rr.append(r) 
ristra = range(t0, tmax+1, step)

for ciclo in ristra:
    stemp = s
    itemp = i
    rtemp = r
    s = ss[-1] - beta*ii[-1]*ss[-1]
    i = ii[-1] + beta*ii[-1]*ss[-1] -gamma*ii[-1]
    r = rr[-1] + gamma*ii[-1]
    ss.append(s) 
    ii.append(i) 
    rr.append(r) 


#print(ss)
#print(ii)
#print(rr)
fig, ax = plt.subplots()
ax.plot(ristra, ss[0:-1])
